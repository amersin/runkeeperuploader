#!/usr/bin/bash

# Some steps already abort execution on error using exitOnError to give more detailed error information
# Just to be sure we also let bash abort execution on any error
set -e

scriptPath=$(dirname "$0")
configfile=$scriptPath/runkeeperUploader.config
email=""
password=""

files2UploadDir=""
processedFilesDir=""
fileFormatFilter="*.gpx"

headers=$scriptPath/headers
loginUrl="https://runkeeper.com/login"
uploadUrl="https://runkeeper.com/trackMultipleFileUpload"
succcessMessage="success"

logFile=""

# exitWithError <exitCode> <message>
function exitWithError {
  echo "$2" >&2
  exit $1
}

# create a log entry, message is appended after the time, and account
# log <log message>
function log {
  echo "$(date) - $email - $1" >> "$logFile"
}

# username, password for login, *.gpx files to be uploaded, and log file path are passed via configuration file
# Log file is a JSON formated file
function readConfig {
  local config=$(cat "$configfile") || exitWithError 1 "File '$configfile' must contain the Runkeeper account information"
  email=$(echo $config | jq -r '.accounts[0].email')
  password=$(echo $config | jq -r '.accounts[0].password')
  files2UploadDir=$(echo $config | jq -r '.accounts[0].folder')
  processedFilesDir=$files2UploadDir/processed
  logFile=$(echo $config | jq -r '.logFile')
}

function processUpload {
  count=`ls -1 $files2UploadDir$fileFormatFilter 2>/dev/null | wc -l` # Count the number of *.gpx files to be uploaded
  if [ $count != 0 ]
  then
    login
    for file in $files2UploadDir$fileFormatFilter; do
      filename=$(basename -- "$file")
      uploadFile "$file" "$filename"  # Upload the file
      done
    logout
  fi
}

# login to RunKeeper web site, exit in case of failure
function login {
  # Post a request to Login Url with username and password, dump the headers, cookies will be needed to authenticate later
  local loggedIn=`curl -s -d "email=$email&password=$password&submit=Login" -w "%{http_code}" --dump-header $headers $loginUrl`
  if [ $loggedIn == 500 ] # HTTP Error 500 is login failed error
  then
    log "Login failed."
    exitWithError 2 "Login failed."
  fi
}

# upload file to Runkeeper
# uploadFile <fullPath> <fileName>
function uploadFile {
  # Post the file to be uploaded, return is a JSON response containing result, and errors if any
  local response=`curl -s -L -b $headers --form "trackFiles=@$1;filename=$2" --form handleUpload=handleUpload $uploadUrl` #
  local result=$(echo $response | jq -r '.result')
  if [ "$result" == "success" ]   # Upload successful
  then
    mv "$1" $processedFilesDir    # Move file to processed folder, to prevent repeated uploads
    log "Activity uploaded $2"
  else
    local error=$(echo $response | jq -r '.errors[0]')
    log "Upload failed for $2 - $error"
  fi
}

# logout from Runkeeper
function logout {
  rm -rf headers  # Remove the headers, need to login each time there is file to be uploaded
}

readConfig
processUpload
